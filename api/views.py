from urllib import unquote
import requests

from django.shortcuts import get_object_or_404
from django.utils.html import escape
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from rest_framework.response import Response
from rest_framework.views import APIView


from proxymon.models import Card, CardsInDeck, Deck, Set
from proxymon.parsers import PTCGOParser
from proxymon.serializers import CardSerializer, DeckSerializer, SetSerializer


class MetaIDCards(APIView):

    def get_object(self, metaid):
        return Card.objects.filter(metaid=metaid)

    def get(self, request, metaid, format=None):
        cards = self.get_object(metaid)
        serializer = CardSerializer(cards, many=True)

        return Response(serializer.data)


class DeckView(APIView):

    def get_object(self, pk):
        return get_object_or_404(Deck, id=pk)

    def get(self, request, id, format=None):
        deck = self.get_object(id)
        serializer = CardSerializer(deck.cards.all(), many=True)

        return Response({
            'cards': serializer.data,
            'name': deck.name,
            'errors': deck.errors
        })


class Featured(APIView):

    def get(self, request, format=None):
        decks = Deck.objects.filter(featured=True)
        serializer = DeckSerializer(decks, many=True)

        return Response(serializer.data)


class CreateDeck(APIView):

    def create_deck(self, decklist):
        deck = Deck()
        deck.save()
        for qty, ptcgo_code, is_energy in decklist:
            if is_energy:
                xy_base = Set.objects.get(code='xy1')
                try:
                    card = Card.objects.get(set=xy_base,
                                            name__startswith=ptcgo_code,
                                            supertype='Energy')
                except Card.DoesNotExist:
                    errors = deck.errors or ''
                    deck.errors = errors + '%s not found\n' % ptcgo_code
                    deck.save()
                    continue
            else:
                try:
                    card = Card.objects.get(ptcgo_code=ptcgo_code)
                except Card.DoesNotExist:
                    errors = deck.errors or ''
                    deck.errors = errors + '%s not found\n' % ptcgo_code
                    deck.save()
                    continue
            for i in xrange(int(qty)):
                cid = CardsInDeck(card=card, deck=deck)
                cid.save()
        return deck

    @method_decorator(csrf_protect)
    def post(self, request, format=None):
        decklist = request.data['decklist']
        name = escape(request.data['name'])
        parser = PTCGOParser(decklist)
        self.deck = self.create_deck(parser.decklist)
        self.deck.decklist = decklist
        self.deck.name = name
        self.deck.save()

        serializer = DeckSerializer(self.deck)

        return Response(serializer.data)


class Search(APIView):

    def get(self, request, q, format=None):

        q = unquote(q)

        cards = Card.objects.filter(name__icontains=q)[:10]

        serializer = CardSerializer(cards, many=True)

        return Response(serializer.data)


class AdminSetAPI(APIView):

    def get(self, request, code, format=None):
        set = Set.objects.get(code=code)
        serializer = SetSerializer(set)

        return Response(serializer.data)


class AdminReloadSetAPI(APIView):

    @method_decorator(csrf_protect)
    def post(self, request, format=None):
        if request.data['prompt'] == 'yes':
            set_code = request.data['code']

            set = Set.objects.get(code=set_code)
            print 'Deleting %s cards from %s' % (set.total_cards, set.name)
            cards = Card.objects.filter(set=set)
            for card in cards:
                card.delete()

            self.reload(set)

        return Response()

    def reload(self, set):
        url = 'http://api.pokemontcg.io/v1/cards/?setCode=%s&pageSize=%s' % (set.code, 500)
        r = requests.get(url)

        data = r.json()['cards']
        for c in data:
            card = Card()
            card.id = c['id']
            card.name = c['name']
            card.imageUrl = c['imageUrl']
            card.imageUrlHiRes = c['imageUrlHiRes']
            card.supertype = c['supertype']
            card.set = set
            card.automatic = True
            card.number = c['number']
            card.ptcgo_code = '%s-%s' % (set.ptcgoCode, card.number)

            card.save()

        print 'Reloaded %s cards for %s' % (len(Card.objects.filter(set=set)), set.name)


class CardAPI(APIView):

    def get(self, request, format=None):
        setcode = request.GET.get('set', '')
        if setcode:
            cards = Card.objects.filter(set__code=setcode).order_by('numeric_sort')
            serializer = CardSerializer(cards, many=True)
            return Response({
                'data': serializer.data
            })
        else:
            pass

    @method_decorator(csrf_protect)
    def post(self, request, format=None):
        new_card = request.data['card']
        card_name = new_card['name']
        card_imageUrl = new_card['imageUrl']
        card_supertype = new_card['supertype']
        card_ptcgo_code = new_card['ptcgo_code']
        card_number = new_card['number']

        set = Set.objects.get(code=request.data['setCode'])

        card = Card()
        card.name = card_name
        card.number = card_number
        card.id = '%s-%s' % (set.code.lower(), card.number)
        card.imageUrl = card_imageUrl
        card.supertype = card_supertype
        card.ptcgo_code = card_ptcgo_code
        card.set = set
        card.automatic = False

        card.save()

        return Response({'status': 'OK'})


class SetAPI(APIView):

    def get(self, request, format=None):
        sets = Set.objects.all()
        serializer = SetSerializer(sets, many=True)
        return Response({
            'data': serializer.data
        })
