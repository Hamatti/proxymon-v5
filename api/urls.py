from django.conf.urls import url

import views

urlpatterns = [
    url(r'^create/', views.CreateDeck.as_view()),
    url(r'^metaid/(?P<metaid>[0-9]+)/', views.MetaIDCards.as_view()),
    url(r'^deck/(?P<id>[0-9]+)/', views.DeckView.as_view()),
    url(r'^search/(?P<q>[a-zA-Z0-9\-\%\ ]+)/', views.Search.as_view()),
    url(r'^featured/', views.Featured.as_view()),
    url(r'^cards/', views.CardAPI.as_view()),
    url(r'^set/reload/', views.AdminReloadSetAPI.as_view()),
    url(r'^set/', views.SetAPI.as_view()),
]


admin_urlpatterns = [
    url(r'^admin/set/(?P<code>[a-zA-Z0-9]+)/', views.AdminSetAPI.as_view()),
]

urlpatterns += admin_urlpatterns
