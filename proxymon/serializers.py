from rest_framework import serializers
from proxymon.models import Card, CardsInDeck, Deck, Set


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = ('id', 'name', 'imageUrl', 'imageUrlHiRes',
                  'supertype', 'metaid')


class DeckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = ('name', 'cards', 'created_at', 'id', 'errors')


class SetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Set
        fields = ('code', 'name', 'series', 'total_cards', 'standard_legal',
                  'expanded_legal', 'symbol_url', 'logo_url')
