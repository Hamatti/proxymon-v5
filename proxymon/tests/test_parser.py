# coding=utf8
import os

from django.test import TestCase

from proxymon.parsers import PTCGOParser,\
                             SET_CARD_PATTERN,\
                             BASIC_ENERGY_PATTERN


class SetCardPatternTest(TestCase):
    """Test cases that cover the SET_CARD_PATTERN regex functionality"""

    def test_pattern_singlename_pokemon_with_star(self):
        card_str = '* 1 Caterpie SUM 1'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '1')
        )

    def test_pattern_singlename_pokemon_without_star(self):
        card_str = '1 Caterpie SUM 1'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '1')
        )

    def test_pattern_singlename_pokemon_promo_sm(self):
        card_str = '* 1 Tapu Koko PR-SM SM31'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'PR-SM', 'SM31')
        )

    def test_pattern_multiname_pokemon_with_star(self):
        card_str = '* 1 Alolan Rattata SUM 76'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '76')
        )

    def test_pattern_multiname_pokemon_without_star(self):
        card_str = '1 Alolan Rattata SUM 76'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '76')
        )

    def test_pattern_singlename_trainer_with_star(self):
        card_str = '* 1 Hau SUM 120'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '120')
        )

    def test_pattern_singlename_trainer_without_star(self):
        card_str = '1 Hau SUM 120'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '120')
        )

    def test_pattern_multiname_trainer_with_star(self):
        card_str = '* 1 Pokémon Catcher SUM 126'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '126')
        )

    def test_pattern_multiname_trainer_without_star(self):
        card_str = '1 Pokémon Catcher SUM 126'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'SUM', '126')
        )

    def test_pattern_quantity(self):
        card_str = '* 3 Pokémon Catcher SUM 126'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('3', 'SUM', '126')
        )


class BasicEnergyPatternTest(TestCase):
    """Test cases that cover the BASIC_ENERGY_PATTERN regex functionality"""

    def test_pattern_basic_energy_darkness(self):
        card_str = '* 1 Darkness Energy  7'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Darkness')
        )

    def test_pattern_basic_energy_fairy(self):
        card_str = '* 1 Fairy Energy  9'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Fairy')
        )

    def test_pattern_basic_energy_fighting(self):
        card_str = '* 1 Fighting Energy  6'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Fighting')
        )

    def test_pattern_basic_energy_fire(self):
        card_str = '* 1 Fire Energy  2'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Fire')
        )

    def test_pattern_basic_energy_grass(self):
        card_str = '* 1 Grass Energy  1'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Grass')
        )

    def test_pattern_basic_energy_lightning(self):
        card_str = '* 1 Lightning Energy  4'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Lightning')
        )

    def test_pattern_basic_energy_metal(self):
        card_str = '* 1 Metal Energy  8'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Metal')
        )

    def test_pattern_basic_energy_psychic(self):
        card_str = '* 1 Psychic Energy  5'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Psychic')
        )

    def test_pattern_basic_energy_water(self):
        card_str = '* 1 Water Energy  3'
        self.assertEqual(
            BASIC_ENERGY_PATTERN.match(card_str).groups(),
            ('1', 'Water')
        )

    def test_pattern_fightingfurybelt(self):
        card_str = '* 1 Fighting Fury Belt BKP 99'
        self.assertEqual(
            SET_CARD_PATTERN.match(card_str).groups(),
            ('1', 'BKP', '99')
        )


class PTCGOParserTest(TestCase):
    """ Test cases that cover end-to-end functionality of the parser"""

    def test_empty_string(self):
        raw_decklist = ''
        parser = PTCGOParser(raw_decklist)
        self.assertEqual(parser.decklist, [])

    def test_single_card(self):
        raw_decklist = '* 3 Pokémon Catcher SUM 126'
        parser = PTCGOParser(raw_decklist)
        self.assertEqual(parser.decklist, [('3', 'SUM-126', False)])

    def test_pokemon_and_energy(self):
        raw_decklist = '* 3 Pokémon Catcher SUM 126\n* 1 Water Energy  3'
        parser = PTCGOParser(raw_decklist)
        self.assertEqual(parser.decklist, [('3', 'SUM-126', False), ('1', 'Water', True)])

    def test_pokemon_and_energy_2(self):
        raw_decklist = '* 1 Water Energy  3\n* 3 Pokémon Catcher SUM 126'
        parser = PTCGOParser(raw_decklist)
        self.assertEqual(parser.decklist, [('1', 'Water', True), ('3', 'SUM-126', False)])

    def test_Flareon_RC_GEN106(self):
        raw_decklist = '* 1 Flareon-EX GEN 106'
        parser = PTCGOParser(raw_decklist)
        self.assertEqual(parser.decklist, [('1', 'GEN-106', False)])

class DeckTest(TestCase):
    """Test cases that use full in-file decklists"""

    def create_parser_with_decklist(self, deckstring):
        parser = PTCGOParser(deckstring)
        return parser

    def test_basic_energies_list(self):
        from proxymon.tests.test_deck_data import basic_energies
        parser = self.create_parser_with_decklist(basic_energies.decklist)
        self.assertEqual(len(parser.decklist), 9)
        self.assertEqual(parser.decklist, basic_energies.output)

    def test_sm1_list(self):
        self.maxDiff = None
        from proxymon.tests.test_deck_data import sm1
        parser = self.create_parser_with_decklist(sm1.decklist)
        self.assertEqual(len(parser.decklist), 135)
        self.assertEqual(parser.decklist, sm1.output)
