from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import AdminTextareaWidget
from .models import Card, Deck, Set


class CardAdmin(admin.ModelAdmin):

    fields = ['id', 'metaid', 'name', 'supertype', 'set',
              'number', 'numeric_sort', 'image_tag', 'ptcgo_code']
    list_display = ['id', 'name', 'supertype', 'set',
                    'numeric_sort', 'metaid', 'ptcgo_code']
    list_filter = ['supertype', 'set__series', 'set__standard_legal',
                   'set__expanded_legal', 'set']

    readonly_fields = ['image_tag', 'number', 'numeric_sort', 'set',
                       'supertype', 'name', 'id']

    search_fields = ['id', 'name', 'supertype', 'numeric_sort', 'ptcgo_code']


class SetAdmin(admin.ModelAdmin):

    list_display = ['code', 'name', 'series', 'total_cards', 'standard_legal',
                    'expanded_legal', 'ptcgoCode']
    list_filter = ['series', 'standard_legal', 'expanded_legal']

    search_fields = ['code', 'name']


class DeckAdmin(admin.ModelAdmin):

    fields = ['name', 'created_at', 'featured', 'errors', 'decklist']
    list_display = ['id', 'name', 'created_at', 'error_count']
    readonly_fields = ['created_at', 'decklist']
    search_fields = ['name']


admin.site.register(Card, CardAdmin)
admin.site.register(Set, SetAdmin)
admin.site.register(Deck, DeckAdmin)
