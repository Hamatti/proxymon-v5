from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import generic

from models import Card, CardsInDeck, Deck, Set
from parsers import PTCGOParser
import collections


class CardListView(generic.ListView):
    model = Card
    template_name = 'cards_list.html'

    def get_queryset(self):
        setcode = self.kwargs['setcode']
        set = Set.objects.get(code=setcode)

        return Card.objects.filter(set=set).order_by('numeric_sort')


class MetaidView(generic.TemplateView):

    template_name = 'metaid.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MetaidView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(MetaidView, self).get_context_data(**kwargs)

        cards_with_metaid = Card.objects.filter(metaid__isnull=False)

        cards = collections.defaultdict(list)
        for card in cards_with_metaid:
            cards[card.metaid].append(card)

        context_data['cards'] = dict(cards)

        return context_data


class SetsAdminToolView(generic.TemplateView):

    template_name = 'tools/sets.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SetsAdminToolView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(SetsAdminToolView, self).get_context_data(**kwargs)
        sets = Set.objects.all()

        context_data['sets'] = sets

        return context_data


class SingleSetAdminToolView(generic.TemplateView):

    template_name = 'tools/set.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SingleSetAdminToolView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(SingleSetAdminToolView, self).get_context_data(**kwargs)
        setcode = kwargs['setcode']

        _set = Set.objects.get(code=setcode)

        context_data['sset'] = _set

        return context_data
