import requests

from django.core.management.base import BaseCommand, CommandError

from proxymon.models import Card, Set


class Command(BaseCommand):
    help = 'Fetch cards from a certain set'

    def add_arguments(self, parser):
        parser.add_argument('card', type=str)
        parser.add_argument('fields', nargs='+', type=str)

    def handle(self, *args, **kwargs):
        self.cardcode = kwargs['card']
        self.fields = kwargs['fields']
        baseurl = 'http://api.pokemontcg.io/v1/cards/%s' % self.cardcode
        try:
            card = Card.objects.get(id=self.cardcode)
            r = requests.get(baseurl)
            data = r.json()['card']

            for field in self.fields:
                print 'Updating field %s in %s' % (field, card.name)
                setattr(card, field, data[field])

            card.save()

        except Card.DoesNotExist:
            print 'Something went wrong'
