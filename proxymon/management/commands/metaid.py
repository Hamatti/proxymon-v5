# coding=utf8
from django.core.management import BaseCommand
from django.db.models import Q

from proxymon.models import Card


class Command(BaseCommand):

    is_null = Q(metaid__isnull=True)

    def add_arguments(self, parser):
        parser.add_argument('supertype', type=str)

    def similar_metaid(self, supertype_Q):
        cards = Card.objects.filter(supertype_Q & self.is_null)

        for card in cards:
            if card.metaid:
                continue
            similar = Card.objects.filter(name=card.name).exclude(id=card.id)
            if len(similar) > 0:
                next_metaid = Card.objects.latest('metaid').metaid + 1
                try:
                    print card, card.imageUrl
                except UnicodeError:
                    print 'Error at %s' % card.id
                for sim in similar:
                    print 'similar with %s, %s ' % (sim, sim.imageUrl)
                    if raw_input() == 'y':
                        card.metaid = next_metaid
                        sim.metaid = next_metaid
                        card.save()
                        sim.save()

    def metaid_trainers(self):
        is_trainer = Q(supertype='Trainer')
        self.similar_metaid(is_trainer)

    def metaid_pokemons(self):
        is_pokemon = Q(supertype='Pokémon')
        self.similar_metaid(is_pokemon)

    def handle(self, *args, **kwargs):
        if kwargs['supertype'] == 'trainer':
            self.metaid_trainers()

        elif kwargs['supertype'] == 'pokemon':
            self.metaid_pokemons()
