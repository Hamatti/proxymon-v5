import requests

from django.core.management.base import BaseCommand, CommandError

from proxymon.models import Card, Set


class Command(BaseCommand):
    help = 'Fetch cards from a certain set'

    def add_arguments(self, parser):
        parser.add_argument('fields', nargs='+', type=str)
        parser.add_argument(
                '--setcode',
                action='store',
                dest='set',
                default='')

    def handle(self, *args, **kwargs):
        if kwargs['set']:
            set = Set.objects.get(code=kwargs['set'])
            cards = Card.objects.filter(set=set)
        else:
            cards = Card.objects.all()
        self.fields = kwargs['fields']

        for card in cards:
            baseurl = 'http://api.pokemontcg.io/v1/cards/%s' % card.id
            r = requests.get(baseurl)
            data = r.json()['card']

            for field in self.fields:
                print 'Updating field %s in %s' % (field, card.name)
                setattr(card, field, data[field])

            card.save()
