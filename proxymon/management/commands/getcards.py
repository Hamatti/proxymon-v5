import requests

from django.core.management.base import BaseCommand, CommandError

from proxymon.models import Card, Set


class Command(BaseCommand):
    help = 'Fetch cards from a certain set'

    def add_arguments(self, parser):
        parser.add_argument('setcode', type=str)
        parser.add_argument(
                '--all',
                action='store_true',
                dest='all',
                default=False,
        )

    def fetch(self, seturl):
        r = requests.get(seturl)
        data = r.json()['set']

        set = Set()
        set.code = self.setcode
        set.ptcgoCode = data['ptcgoCode']
        set.name = data['name']
        set.series = data['series']
        set.total_cards = data['totalCards']
        set.standard_legal = data['standardLegal']
        set.expanded_legal = data['expandedLegal']
        set.symbol_url = data['symbolUrl']
        set.logo_url = data['logoUrl']

        set.save()

    def all_cards(self):
        sets = Set.objects.all()
        baseurl = 'http://api.pokemontcg.io/v1/cards/?setCode=%s&pageSize=%s'
        for set in sets:
            if Card.objects.filter(set=set):
                print 'Cards on %s already loaded' % set.name
                continue

            print 'Loading cards from %s' % set.name
            seturl = baseurl % (set.code, 300)
            r = requests.get(seturl)

            data = r.json()

            cards = data['cards']
            for c in cards:
                card = Card()
                card.id = c['id']
                card.name = c['name']
                card.imageUrl = c['imageUrl']
                card.imageUrlHiRes = c['imageUrlHiRes']
                card.supertype = c['supertype']
                card.set = set

                card.save()

    def handle(self, *args, **kwargs):
        if kwargs['all']:
            self.all_cards()
            return
        baseurl = 'http://api.pokemontcg.io/v1/cards/?setCode=%s&pageSize=%s'
        self.setcode = kwargs['setcode']

        set = Set.objects.get(code=self.setcode)
        try:
            cards = Card.objects.get(set=set)
            print '%s cards from set %s already loaded' % (len(cards),
                                                           self.setcode)
        except Card.DoesNotExist:
            seturl = baseurl % (self.setcode, 300)
            r = requests.get(seturl)
            data = r.json()

            cards = data['cards']
            for c in cards:
                card = Card()
                card.id = c['id']
                card.name = c['name']
                card.imageUrl = c['imageUrl']
                card.imageUrlHiRes = c['imageUrlHiRes']
                card.supertype = c['supertype']
                card.set = set
                card.number = c['number']
                print card.number

                card.save()
