import requests

from django.core.management.base import BaseCommand, CommandError

from proxymon.models import Set


class Command(BaseCommand):
    help = 'Fetch cards from a certain set'

    def add_arguments(self, parser):
        parser.add_argument('setcode', type=str)

    def fetch(self, seturl):
        r = requests.get(seturl)
        data = r.json()['set']

        set = Set()
        set.code = self.setcode
        set.ptcgoCode = data['ptcgoCode']
        set.name = data['name']
        set.series = data['series']
        set.total_cards = data['totalCards']
        set.standard_legal = data['standardLegal']
        set.expanded_legal = data['expandedLegal']
        set.symbol_url = data['symbolUrl']
        set.logo_url = data['logoUrl']

        set.save()

    def handle(self, *args, **kwargs):
        baseurl = 'http://api.pokemontcg.io/v1/sets/%s'
        self.setcode = kwargs['setcode']
        seturl = baseurl % self.setcode
        try:
            Set.objects.get(code=self.setcode)
            print '%s is already loaded' % self.setcode

        except Set.DoesNotExist:
            self.fetch(seturl)
