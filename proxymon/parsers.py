import re

# SET_CARD_PATTERN REGEX explained
# (?:\* )? :: optional * in the start of the line -> ignore
# (\d+) :: 1 or more numbers -> quantity
#  .* :: any number of characters between quantity and set code -> ignore
# ([A-Z]{2,3}|[A-Z]{2}-[A-Z]{2}|[A-Z0-9]{3})? :: -> setcode
# (\d+|XY\d+|BW\d+|SM\d+) :: either a number or [SET][NUMBER] for promos -> setnro

SET_CARD_PATTERN = re.compile(r'(?:\* )?(\d+) .* ([A-Z]{2,3}|[A-Z]{2}-[A-Z]{2}|[A-Z0-9]{3}) (\d+|XY\d+|BW\d+|SM\d+)')

BASIC_ENERGY_PATTERN = re.compile(r'(?:\* )?(\d+) (Grass|Lightning|Fire|Fairy|Darkness|Metal|Fighting|Psychic|Water)')


class PTCGOParser(object):

    def __init__(self, decklist):
        self.raw_decklist = decklist
        self.decklist = []
        self.create()

    def _is_card(self, line):
        return SET_CARD_PATTERN.match(line) or BASIC_ENERGY_PATTERN.match(line)

    def convert_to_tuple(self, line):
        set_card_match = SET_CARD_PATTERN.match(line)
        basic_energy_match = BASIC_ENERGY_PATTERN.match(line)

        if set_card_match:
            quantity, set_name, set_nro = set_card_match.groups()
            is_energy = False
            # Promo cards have a different coding
            if set_name.startswith('PR-'):
                ptcgo_code = '%s %s' % (set_name, set_nro)
            else:
                ptcgo_code = '%s-%s' % (set_name, set_nro)
        elif basic_energy_match:
            (quantity, ptcgo_code), is_energy = basic_energy_match.groups(), True

        return quantity, ptcgo_code, is_energy

    def create(self):
        for line in self.raw_decklist.split('\n'):
            line = line.strip()
            if self._is_card(line):
                quantity, card, is_energy = self.convert_to_tuple(line)
                self.decklist.append((quantity, card, is_energy))
