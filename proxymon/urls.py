"""proxymon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView

import views

urlpatterns = [
    url(r'^admin/?', admin.site.urls),
    url(r'^admin/cards(?P<setcode>[A-Za-z0-9]+)/',
        views.CardListView.as_view(), name='cards'),
    url(r'^admin/metaid', views.MetaidView.as_view()),
    url(r'^admin/tools/sets', views.SetsAdminToolView.as_view()),
    url(r'^admin/tools/set/(?P<setcode>[A-Za-z0-9]+)', views.SingleSetAdminToolView.as_view(), name='tools_set'),

    url(r'^api/', include('api.urls')),


    # Frontend managed by Vue.js router
    url(r'^.*$', TemplateView.as_view(template_name="index.html"))
]
