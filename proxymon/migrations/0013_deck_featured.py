# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-09 05:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proxymon', '0012_deck_decklist'),
    ]

    operations = [
        migrations.AddField(
            model_name='deck',
            name='featured',
            field=models.BooleanField(default=False),
        ),
    ]
