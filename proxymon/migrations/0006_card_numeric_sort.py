# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-24 16:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proxymon', '0005_auto_20171124_1558'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='numeric_sort',
            field=models.IntegerField(null=True),
        ),
    ]
