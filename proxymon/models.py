from django.db import models
from django.utils.html import mark_safe
import re


class Set(models.Model):

    code = models.CharField(max_length=10)
    ptcgoCode = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    series = models.CharField(max_length=200)
    total_cards = models.IntegerField(blank=True)
    standard_legal = models.BooleanField(default=False)
    expanded_legal = models.BooleanField(default=False)
    symbol_url = models.URLField()
    logo_url = models.URLField()

    def __unicode__(self):
        return '%s:%s' % (self.name, self.code)


class Card(models.Model):

    id = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=200)
    imageUrl = models.URLField()
    imageUrlHiRes = models.URLField()
    supertype = models.CharField(max_length=200)
    set = models.ForeignKey(Set)
    number = models.CharField(max_length=200, null=True)
    numeric_sort = models.IntegerField(null=True)
    metaid = models.IntegerField(null=True, blank=True)
    automatic = models.BooleanField(default=False)

    ptcgo_code = models.CharField(max_length=200, null=False, default='')

    def image_tag(self):
        return mark_safe('<img src="%s" width="300"' % self.imageUrl)

    image_tag.short_description = 'Image'

    def __unicode__(self):
        return '%s: %s' % (self.id, self.name)

    def save(self, *args, **kwargs):
        try:
            self.numeric_sort = int(re.match(r'[A-Za-z]*([0-9]+)',
                                    self.number).groups()[0])
        except TypeError:
            self.numeric_sort = self.number
        super(Card, self).save(*args, **kwargs)


class Deck(models.Model):

    name = models.CharField(max_length=200, null=True)
    cards = models.ManyToManyField(Card, through='CardsInDeck')
    created_at = models.DateTimeField(auto_now_add=True)
    errors = models.CharField(max_length=800, null=True, blank=True, default='')
    decklist = models.CharField(max_length=300000, blank=True, null=True, default='')
    featured = models.BooleanField(default=False)

    def error_count(self):
        if self.errors:
            return len(self.errors.split(','))
        else:
            return 0

    def _pprint(self):
        for card in self.cards.all():
            print card

    def __unicode__(self):
        return '%s (%s cards)' % (self.name, len(self.cards.all()))


class CardsInDeck(models.Model):
    deck = models.ForeignKey(Deck)
    card = models.ForeignKey(Card)

    def __unicode__(self):
        return '%s: %s (%s)' % (self.deck.name, self.card.name, self.card.id)
