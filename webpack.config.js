var path = require('path');
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker')


module.exports = {
    context: __dirname,
    entry: './frontend/js/index',
    output: {
        path: path.resolve('./frontend/bundles/'),
        filename: 'app.js'
    },

    plugins: [
      new BundleTracker({filename: './webpack-stats.json'}),
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ],
    },
    resolve: {
        alias: {vue: 'vue/dist/vue.js'}
    },

};
