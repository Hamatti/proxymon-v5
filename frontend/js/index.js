window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
import axios from 'axios';
import Vue from 'vue';
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import underscore from 'vue-underscore';

import App from './components/App.vue'
import DeckView from './components/DeckView.vue'
import PageNotFound from './components/PageNotFound.vue'
import Search from './components/Search.vue'
import Featured from './components/Featured.vue'
import SetAdmin from './components/SetAdmin.vue'
import AdminToolSets from './components/AdminToolSets.vue'

Vue.use(VueAxios, axios)
Vue.use(VueRouter)
Vue.use(underscore)

// Required for Django POST requests
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";

const routes = [
  { path: '/', component: App },
  { path: '/deck/:id', component: DeckView, props: true },
  { path: '/search', component: Search },
  { path: '/featured', component: Featured },
  { path: '/admin/tools/sets/', component: AdminToolSets, props: true},
  { path: '/admin/tools/set/:code', component: SetAdmin, props: true, name: 'set'},
  { path: '*', component: PageNotFound },
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

const app = new Vue({
  router
}).$mount('#app')
